---
title: "Fields Config "
date: 2018-07-11T11:21:23+03:00
  weight = 40
---

You can distinguish between different types of fields and types:
 
## ID
```php
{
          "title": "Id",
          "name": "id",
          "dbType": {
            "type": "increments",
            "primary": true
          }
        },
```

##  Integer
```php
{
        
          "title": "Integer Value",
          "name": "int_value",
          "dbType": {
            "type": "Integer",
            ....
          },
          "viewType": {
            "type": "Integer"
          },
         ..........
        }
```
## double
```php
{
          "title": "Double Value",
          "name": "double_value",
          "dbType": {
            "type": "double",
          ...
          },
          "viewType": {
            "type": "decimal",
            ...
          },
          ...
        }
```
##  default
```php
{
        
	   .........
          "dbType": {
            "type": "...",
            "default": "default value"
          },
          "viewType": {
         ..........
          },
         ............
        }
```
##  foreign
 ```php
 	{
          "title": "field Name",
          "name": "field_id",
          "dbType": {
            "type": "Integer",
            "foreign": {
                "relatedEntity": "related entity name",
                "fieldView": "field_name",
             ...
            }
          },
          "viewType": {
            "type": "text"
          },
        .....
        }
  ```


## enum

  
 ```php
     {
       ....
          "dbType": {
            "type": "enum"
          },
          "viewType": {
              "type": "select",
            "enums": [
              {
                "label": "Label Value1",
                "value": "val1"
              },
              {
                "label": "Label Value2",
                "value": "val2"
              }
              {
              ....
              }
            ]
          },
        ....
        }
  ```
  
  

  
##   validations    

  - ``` required ```
   
      ```php
       "validations": "required|...", 
       ```
 
  - ``` maximum ```
 
  
      ```php
     "validations": "max:max_value|...",
      ```
 
 - ``` minimum ```
    
     ```php
      "validations": "min:min_value|...",
     ```
 - ``` unique ```
    
     ```php
      "validations": "unique:tabel_name|...",
     ```
 - ``` date ```
    
     ```php
      "validations": "date|...",
     ```

 - ``` file ```
    
     ```php
      "validations": "file|...",
     ```
 - for more validations see [this] (https://laravel.com/docs/5.7/validation#rule-same)
     
      
## ``` searchable```      
     
      
      { 
        ....
        "searchable": "true",
        ....
       }

## ``` sortable```      
     
      
      { 
        ....
        "sortable": "true",
        ....
       }
      
## ``` fillable```      
     
     
      { 
        ....
        "fillable": "true",
        ....
       }
     
## ``` inForm```      
     
    
      { 
        ....
        "inForm": "true",
        ....
       }
    
## ``` inIndex```      
     
     
      { 
        ....
       "inIndex": "true",
       ....
       }
      
## ``` inView```      
     
     
      { 
        ....
       "inIndex": "true",
       ....
       }      
       
##   view type
   - ```text``` :
   
       ```php
       "viewType": {
             "type": "text"
                 },
       ```
    
   - ```textarea``` :
   
       ```php
       "viewType": {
             "type": "textarea"
                 },
       ```
   - ```password``` :
   
       ```php
       "viewType": {
             "type": "password"
                 },
       ```
   - ```hidden``` :
   
       ```php
       "viewType": {
             "type": "hidden"
                 },
       ```
   - ```email``` :
   
       ```php
       "viewType": {
             "type": "email"
                 },
       ```
   - ```date``` :
   
       ```php
       "viewType": {
             "type": "date"
                 },
       ```
   - ```select``` :
   
       ```php
       "viewType": {
             "type": "select"
                 },
       ```
 

   - ```ckeditor``` :
   
       ```php
       "viewType": {
             "type": "ckeditor"
                 },
       ```
   - ```token``` :
   
       ```php
       "viewType": {
             "type": "token"
                 },
       ```
       
   - ```Image``` :
   
       ```php
       "viewType": {
             "type": "img"
                 },
       ```