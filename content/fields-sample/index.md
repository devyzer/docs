---
title: "Fields Sample"
date: 2018-07-11T14:19:46+03:00
   weight = 50
---

## Entity Sample
this is simple sample ```Customer```
```json
      {
        "name": "customer",
        "model": "Customer",
        "table": "customers",
        "briefTitle": "Customers",
        "fields": [
  	  	       {
            "title": "Id",
            "name": "id",
            "dbType": {
              "type": "increments",
              "primary": true
  			
            },
            "viewType": {
            
            },
  		  
            "searchable": false,
            "sortable": true,
            "fillable": false,
            "inView": true,
            "inForm": false,
            "inIndex": false
          },
          {
            "title": "Name",
            "name": "name",
            "dbType": {
              "type": "string"
            },
            "viewType": {
              "type": "text"
   
            },
            "validations": "required|min:4",
            "searchable": true,
            "sortable": true,
            "fillable": true,
            "inForm": true,
            "inIndex": true
          },
  			       {
            "title": "Email",
            "name": "email",
            "dbType": {
              "type": "string"
            
            },
            "viewType": {
              "type": "email"
   
            },
            "validations": "required",
            "searchable": false,
            "fillable": true,
            "inForm": true,
            "inIndex": true
          },
  			        {
            "title": "Age",
            "name": "age",
            "dbType": {
              "type": "Integer",
              "default": "18"
            },
            "viewType": {
              "type": "Integer"
   
            },
            "validations": "required|min:10",
            "searchable": false,
            "sortable": true,
            "fillable": true,
            "inForm": true,
            "inIndex": true
          },
  		     {
            "title": "Title",
            "name": "title",
            "dbType": {
              "type": "string"
            
            },
            "viewType": {
              "type": "textarea"
   
            },
            "validations": "",
            "searchable": false,
            "fillable": true,
            "inForm": false,
            "inIndex": false
          },
  				{
            "title": "Balance",
            "name": "balance",
            "dbType": {
              "type": "double"
            
            },
            "viewType": {
              "type": "decimal"
   
            },        
            "searchable": false,
            "fillable": true,
            "inForm": true,
            "inIndex": true
          }
        ],
        "relations": [	
        ],
        "extraProperties": {
          "softdelete": true,
          "metable": true,
          "timestamps": true,
          "cachable": true,
          "sluggable": "name ,slug"
        },
  	  "generation":{
  	  
        "model": true,
        "createEvent": true,
        "updateEvent": true,
        "deleteEvent": true,
        "listener": true,
        "repository": true,
        "createTable": true,
        "createRequest": true,
        "updateRequest": true,
        "controller": true,
        "route": true,
        "breadcrumbs": true,
        "lang": true,
        "view": true,
        "apiCreateRequest": true,
        "apiUpdateRequest": true,
        "apiController": true,
        "apiRoute": true
  	  
  	  }
  	  
  	}

```

