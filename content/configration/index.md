---
title: " File Config"
date: 2018-07-10T21:53:36+03:00
weight: 30
---

 ## Multiple Entities
 You can generate  many entities as the following sample:
 
```json 
{
  "entities": [
    {
       // data of first entity
    }
    {
       // data of second entity
    }
    ]
}
```
## Hello World
 You can generate  many entities as the following sample:
 
```json 
{
  "entities": [
    {
      "name":"hello world"
    }
  ]
}
```

## Entity Content
Inside every entity, you must define the following keys and values:

### Name:
 - name: name of entity 
 
### Model:
 - model: name of model 
 
### Table:
  - table: name of table 
  
### Brief Title:
  - briefTitle: Title of Entity 


### Fields: 
Contain fields of the entity:

 -  title: label name
 - name: field name
 - dbType:
       - type: ```increments```,```Integer```, ```string```, ```double```, ```enum```, ```blob```  ... [etc]( https://www.tkserver.com/laravel-database-column-types)
       - primary: ```true```, ```false```
       - default: default value
       - foreign : foreign key: 
             
          - relatedEntity: name of related entity
          - fieldView: the field that you want to view it in the related entity
                
 - viewType:
    - type: ```Integer```,```text```,```textarea```,```password```,```hidden```,  ```date```, ```select```,  ```ckeditor``` ,  ```token```,  ```Image``` ...etc
 - validations:  ```required```, ```email```,```min:value```,```max:value```   ...[etc] (https://laravel.com/docs/5.7/validation#rule-same)
 - searchable: ```true```, ```false``` 
 - sortable: ```true```, ```false``` 
 - fillable: ```true```, ```false```
 - inForm: ```true```, ```false```
 - inIndex: ```true```, ```false```
 - inView: ```true```, ```false```
 
###  Relations: 

 - name: relation name
 - relation:
      - type: ```mtm```,   ```1tm```, 
      - relatedEntity: name of the related entity
       if the type is a ```mtm``` must  determine this fields:
          - middleEntity : name of the middle entity
          - pivotFields :array of middle entity fields
           
      - fieldView:he field that you want to view it in the related entity
      - relatedEntity: name of the related table
      - foreignKey: name of foreign Key field for current entity
      - otherKey: name of foreign Key field for related entity
 
 - inForm: ```true```, ```false```
 - inIndex: ```true```, ```false```
 - inView: ```true```, ```false```
               
### Extra Properties: 

  - softdelete: ```true```, ```false```
  - metable: ```true```, ```false```
  - cachable: ```true```, ```false```
  - timestamp: ```true```, ```false```
  - sluggable: ```field name```,```slug``` or ``` null``` 
     


### Generation: 
   - model :  ```true```, ```false```
   - createEvent :  ```true```, ```false```
   - updateEvent :  ```true```, ```false```
   - deleteEvent :  ```true```, ```false```
   - listener : ```true```, ```false```
   - repository : ```true```, ```false```
   - createTable : ```true```, ```false```
   - createRequest : ```true```, ```false```
   - updateRequest : ```true```, ```false```
   - controller : ```true```, ```false```
   - route : ```true```, ```false```
   - breadcrumbs : ```true```, ```false```
   - lang : ```true```, ```false```
   - view : ```true```, ```false```
   - apiCreateRequest : ```true```, ```false```
   - apiUpdateRequest : ```true```, ```false```
   - apiController : ```true```, ```false```
   - apiRoute : ```true```, ```false```
    



